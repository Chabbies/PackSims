
# Usage inside a Discord Bot:

```
!b sv open [choice] [pack_amount=1]

<packName> (pack amount)
Sends the cards opened to the message channel.
```

### Example Uses:

**Command Usage:**

![roll ex](images/rollexample.png)

The **output files** can then viewed by themselves:

**Rolling once**:

![roll 1](images/roll1.png)

**Rolling twice** (up to 10 rolls at once):

![roll 2](images/roll2.png)
